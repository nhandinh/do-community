package com.codecamp.community.job

import org.quartz.*

final class JobUtil {

  static JobDetail buildJobDetail(Long id) {
    JobDataMap jobDataMap = new JobDataMap()
    jobDataMap.put('id', id)
    return JobBuilder.newJob(UndoJob.class)
        .withIdentity(UUID.randomUUID().toString(), "undo-jobs")
        .withDescription("Undo data")
        .usingJobData(jobDataMap)
        .storeDurably()
        .build()
  }

  static Trigger buildJobTrigger(JobDetail jobDetail, Date date) {
    return TriggerBuilder.newTrigger()
        .forJob(jobDetail)
        .withIdentity(jobDetail.getKey().getName(), "undo-triggers")
        .withDescription("Undo Trigger")
        .startAt(date)
        .withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow())
        .build();
  }

}
