package com.codecamp.community.job

import com.codecamp.community.services.CommunityService
import org.quartz.JobDataMap
import org.quartz.JobExecutionContext
import org.quartz.JobExecutionException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.quartz.QuartzJobBean
import org.springframework.stereotype.Component

@Component
class UndoJob extends QuartzJobBean {

  @Autowired
  CommunityService communityService

  @Override
  protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
    JobDataMap data = context.getJobDetail().jobDataMap
    this.communityService.deleteCommunity(Long.parseLong(data.get('id').toString()))
  }
}
