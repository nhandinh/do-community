package com.codecamp.community.dto

import javax.validation.constraints.NotEmpty

class PostRequest {

  @NotEmpty(message = 'please provide title')
  String title

  @NotEmpty(message = 'please provide content')
  String content
}
