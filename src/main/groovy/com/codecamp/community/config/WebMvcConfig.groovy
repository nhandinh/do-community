package com.codecamp.community.config

import com.codecamp.common.config.AppProperties
import com.codecamp.common.config.MvcConfig
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class WebMvcConfig extends MvcConfig {

  @Bean
  @ConfigurationProperties('app')
  AppProperties appProperties() {
    return new AppProperties()
  }

}
