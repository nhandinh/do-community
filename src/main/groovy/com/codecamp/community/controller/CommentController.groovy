package com.codecamp.community.controller

import com.codecamp.common.domains.Comment
import com.codecamp.common.dto.Result
import com.codecamp.community.services.impl.CommentServiceImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

import javax.validation.Valid

@RestController
@RequestMapping(value = 'comments')
class CommentController {

    @Autowired
    CommentServiceImpl commentService

    @GetMapping(value = '{id}')
    ResponseEntity<Result<Page<Comment>>> getAllComment(@PathVariable('id') Long id,
        @RequestParam(value = 'page', defaultValue = '0') Integer page,
        @RequestParam(value = 'limit', defaultValue = '5') Integer limit) {
        Page<Comment> data = commentService.findCommentByPost(id,page, limit)
        ResponseEntity.ok(new Result<Page<Comment>>().success(data))
    }

    @GetMapping(value = '/child-comments/{parentId}')
    ResponseEntity<Result<Page<Comment>>> getAllChildComment(@PathVariable('parentId') Long parentId,
                                                             @RequestParam(value = 'page', defaultValue = '0') Integer page,
                                                             @RequestParam(value = 'limit', defaultValue = '5') Integer limit) {
        Page<Comment> data = commentService.findAllChildCommentByParentId(parentId, page, limit)
        ResponseEntity.ok(new Result<Comment>().success(data))
    }

    @GetMapping(value = 'detail/{id}')
    ResponseEntity<Result<Comment>> getCommentById(@PathVariable(name = 'id') Long id) {
        Comment comment = this.commentService.findCommentById(id)
        ResponseEntity.ok(new Result<Comment>().success(comment))
    }

    @PostMapping
    ResponseEntity<Result<Comment>> createComment(@Valid @RequestBody Comment comment) {
        comment.id == 1 ? comment.parentId=null : comment.parentId
        Comment com = commentService.saveComment(comment)
        ResponseEntity.ok(new Result<Comment>().success(com))
    }

    @DeleteMapping(value = '/{id}')
    ResponseEntity<Result> deleteComment(@PathVariable(name = 'id') Long id) {
        commentService.deleteComment(id)
        ResponseEntity.ok(new Result<>().success())
    }

    @PutMapping(value = '{id}')
    ResponseEntity<Result<Comment>> updateComment(@PathVariable('id') Long id,
                                           @Valid @RequestBody Comment comment) {
        Comment com = this.commentService.updateComment(id, comment)
        ResponseEntity.ok(new Result<Comment>().success(com))
    }
}
