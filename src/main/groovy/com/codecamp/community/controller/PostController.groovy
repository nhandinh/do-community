package com.codecamp.community.controller

import com.codecamp.common.auth.annotation.CurrentUser
import com.codecamp.common.domains.Post
import com.codecamp.common.dto.Result
import com.codecamp.common.utils.UserPrincipal
import com.codecamp.community.dto.PostRequest
import com.codecamp.community.services.PostService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

import javax.validation.Valid

@RestController
@RequestMapping('post')
class PostController {

  @Autowired
  PostService postService

  @PostMapping(value = '{communityId}/{tagId}')
  ResponseEntity<Result<Post>> addPost(@PathVariable('communityId') Long communityId,
                                       @PathVariable('tagId') Long tagId,
                                       @Valid @RequestBody PostRequest postRequest) {
    Post post = this.postService.addPost(communityId, tagId, postRequest)
    return ResponseEntity.ok(new Result<>().success(post))
  }

  @GetMapping(value = 'detail/{id}')
  ResponseEntity<Result<Post>> getPostDetail(@PathVariable('id') Long id) {
    Post post = this.postService.getPostDetail(id)
    ResponseEntity.ok(new Result<Post>().success(post))
  }

  @GetMapping(value = '{id}')
  ResponseEntity<Result<Page<Post>>> getAllPost(@PathVariable('id') Long id,
                                                @RequestParam(value = 'page', defaultValue = '0') Integer page,
                                                @RequestParam(value = 'limit', defaultValue = '5') Integer limit) {
    Page<Post> posts = this.postService.getPosts(id, PageRequest.of(page, limit))
    ResponseEntity.ok(new Result<Post>().success(posts))
  }

  @GetMapping(value = '{slug}/slug')
  ResponseEntity<Result<Post>> getPostBySlug(@PathVariable('slug') String slug) {
    Post post = this.postService.getPostBySlug(slug)
    ResponseEntity.ok(new Result<Post>().success(post))
  }

  @GetMapping
  ResponseEntity<Result<Page<Post>>> getAllForHome(@CurrentUser UserPrincipal userPrincipal,
                                                   @RequestParam(value = 'page', defaultValue = '0') Integer page,
                                                   @RequestParam(value = 'limit', defaultValue = '5') Integer limit) {
    Page<Post> posts = this.postService.getPostForHome(userPrincipal.id,PageRequest.of(page, limit))
    ResponseEntity.ok(new Result<Page<Post>>().success(posts))
  }
}
