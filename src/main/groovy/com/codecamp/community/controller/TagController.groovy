package com.codecamp.community.controller

import com.codecamp.common.domains.Tag
import com.codecamp.common.dto.Result
import com.codecamp.community.services.TagService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping('{communityId}/tag')
class TagController {

  @Autowired
  TagService tagService

  @PostMapping
  ResponseEntity<Result<Tag>> addTag(@PathVariable('communityId') Long communityId,
                                     @RequestBody Tag tag) {
    Tag tagData = this.tagService.addTag(tag, communityId)
    ResponseEntity.ok(new Result<>().success(tagData))
  }

  @GetMapping
  ResponseEntity<Result<List<Tag>>> getTagsByCommunity(@PathVariable('communityId') Long communityId) {
    List<Tag> tags = this.tagService.getTags(communityId)
    ResponseEntity.ok(new Result<List<Result>>().success(tags))
  }
}
