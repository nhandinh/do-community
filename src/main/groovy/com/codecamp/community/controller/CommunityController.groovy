package com.codecamp.community.controller

import com.codecamp.common.auth.annotation.CurrentUser
import com.codecamp.common.domains.Community
import com.codecamp.common.domains.User
import com.codecamp.common.domains.UserCommunity
import com.codecamp.common.dto.Result
import com.codecamp.common.utils.UserPrincipal
import com.codecamp.community.job.JobUtil
import com.codecamp.community.services.impl.CommunityServiceImpl
import org.quartz.JobDetail
import org.quartz.Scheduler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

import javax.validation.Valid

@RestController
@RequestMapping(value = 'community')
class CommunityController {

  @Autowired
  CommunityServiceImpl communityService

  @Autowired
  Scheduler scheduler


  @GetMapping
  ResponseEntity<Result<Page<Community>>> getAllCommunity(@RequestParam(value = 'page', defaultValue = '0') Integer page,
                                                          @RequestParam(value = 'limit', defaultValue = '5') Integer limit) {
    Page<Community> data = communityService.findAll(page, limit)
    ResponseEntity.ok(new Result<Page<Community>>().success(data))
  }

  @GetMapping(value = '/{id}')
  ResponseEntity<Result<Community>> getCommunityById(@PathVariable(name = 'id') Long id) {
    Community community = this.communityService.findCommunityById(id)
    ResponseEntity.ok(new Result<Community>().success(community))
  }

  @PostMapping
  ResponseEntity<Result<Community>> createCommunity(@Valid @RequestBody Community community,
                                                    @CurrentUser UserPrincipal userPrincipal) {
    Community com = communityService.saveCommunity(community, userPrincipal.id)
    ResponseEntity.ok(new Result<Community>().success(com))
  }

  @DeleteMapping(value = '/{id}')
  ResponseEntity<Result> deleteCommunity(@PathVariable(name = 'id') Long id) {
    communityService.updateStatusDeActive(id)
    JobDetail jobDetail = JobUtil.buildJobDetail(id)
    Calendar calendar = Calendar.getInstance()
    calendar.add(Calendar.MINUTE, 1)
    this.scheduler.scheduleJob(jobDetail, JobUtil.buildJobTrigger(jobDetail, calendar.getTime()))
    ResponseEntity.ok(new Result<>().success())
  }

  @PutMapping(value = '{id}')
  ResponseEntity<Result<Community>> updateCommunity(@PathVariable('id') Long id,
                                                    @Valid @RequestBody Community community) {
    Community com = this.communityService.updateCommunity(id, community)
    ResponseEntity.ok(new Result<Community>().success(com))
  }

  @GetMapping(value = 'slug/{slug}')
  ResponseEntity<Result<Community>> getCommunityBySlug(@PathVariable('slug') String slug) {
    Community com = this.communityService.findBySlug(slug)
    ResponseEntity.ok(new Result<Community>().success(com))
  }

  @PutMapping(value = 'undo/{id}')
  ResponseEntity<Result<Community>> undoCommunity(@PathVariable('id') Long id) {
    Community com = this.communityService.undoCommunity(id)
    ResponseEntity.ok(new Result<Community>().success(com))
  }

  @PostMapping(value = 'join/{id}')
  ResponseEntity<Result<Community>> joinCommunity(@PathVariable('id') Long id, @CurrentUser UserPrincipal userPrincipal) {
    UserCommunity com = this.communityService.joinCommunity(id, userPrincipal.id)
    ResponseEntity.ok(new Result<Community>().success(com))
  }

  @GetMapping(value = 'users/{id}')
  ResponseEntity<Result<List<User>>> getUserOfCommunity(@PathVariable('id') Long id) {
    List<UserCommunity> users = this.communityService.getUsersByCommunity(id)
    ResponseEntity.ok(new Result<List<UserCommunity>>().success(users))
  }

}
