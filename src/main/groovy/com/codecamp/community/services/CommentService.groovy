package com.codecamp.community.services

import com.codecamp.common.domains.Comment
import com.codecamp.common.exception.ResourceNotFoundException
import org.springframework.data.domain.Page

interface CommentService {
    Comment findCommentById(Long id)

    Comment saveComment(Comment comment) throws ResourceNotFoundException

    void deleteComment(Long id)

    Page<Comment> findCommentByPost(Long postId, int page, int limit)

    Page<Comment> findAllChildCommentByParentId(Long parentId, int page, int limit)

    Comment updateComment(Long id, Comment comment)
}
