package com.codecamp.community.services

import com.codecamp.common.domains.Post
import com.codecamp.community.dto.PostRequest
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface PostService {

  Post addPost(Long communityId, Long tagId, PostRequest request)

  Post getPostDetail(Long id)

  Page<Post> getPosts(Long communityId, Pageable Pageable)

  Post getPostBySlug(String slug)

  Page<Post> getPostForHome(Long userId, Pageable Pageable)
}