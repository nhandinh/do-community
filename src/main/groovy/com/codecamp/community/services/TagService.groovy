package com.codecamp.community.services

import com.codecamp.common.domains.Tag


interface TagService {

  Tag addTag(Tag tag, Long id)

  List<Tag> getTags(Long id)
}