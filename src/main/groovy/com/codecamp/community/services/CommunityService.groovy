package com.codecamp.community.services

import com.codecamp.common.domains.Community
import com.codecamp.common.domains.User
import com.codecamp.common.domains.UserCommunity
import org.springframework.data.domain.Page

interface CommunityService {

  Community findCommunityById(Long id)

  Community saveCommunity(Community community, Long userId)

  Community updateStatusDeActive(Long id)

  Page<Community> findAll(int page, int limit)

  Community updateCommunity(Long id, Community community)

  Community findBySlug(String slug)

  Community undoCommunity(Long id)

  void deleteCommunity(Long id)

  UserCommunity joinCommunity(Long id, Long userId)

  List<UserCommunity> getUsersByCommunity(Long communityId)
}