package com.codecamp.community.services.impl

import com.codecamp.common.domains.Comment
import com.codecamp.common.exception.ResourceNotFoundException
import com.codecamp.community.repository.CommentRepository
import com.codecamp.community.services.CommentService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service

import java.awt.print.Pageable

@Service
class CommentServiceImpl implements CommentService {

    @Autowired
    CommentRepository commentRepository

    @Override
    Comment findCommentById(Long id) {
        commentRepository.findById(id)
                .orElseThrow({-> new ResourceNotFoundException("Comment not found ${id}")})
    }

    @Override
    Comment saveComment(Comment comment) throws ResourceNotFoundException {
        Optional<Comment> commentOptional = commentRepository.findById(comment.id)
        commentOptional.ifPresent {
            it -> throw new IllegalAccessException("Comment id: ${it.id} exists")
        }
        commentRepository.save(comment)
    }

    @Override
    void deleteComment(Long id) {
        commentRepository.findById(id)
                .orElseThrow {
                    -> new ResourceNotFoundException("Comment not found ${id}")
                }
        commentRepository.deleteById(id)
    }

    @Override
    Page<Comment> findCommentByPost(Long postId, int page, int limit) {
        return this.commentRepository.findByPost_IdAndParentIdNotNullOrderByCreateDateDesc(postId,
            PageRequest.of(page, limit))
    }

    @Override
    Page<Comment> findAllChildCommentByParentId(Long parentId, int page, int limit) {
        commentRepository.findCommentByParentId(parentId, PageRequest.of(page, limit) as Pageable)
    }
    @Override
    Comment updateComment(Long id, Comment comment) {
        Comment com = this.commentRepository.findById(id)
           .orElseThrow {
               -> new ResourceNotFoundException("Comment not found id ${id}")
           }
        commentRepository.save(com)
        com
    }
}
