package com.codecamp.community.services.impl

import com.codecamp.common.domains.Community
import com.codecamp.common.domains.Post
import com.codecamp.common.domains.Tag
import com.codecamp.common.domains.UserCommunity
import com.codecamp.common.exception.ResourceNotFoundException
import com.codecamp.common.utils.AppUtil
import com.codecamp.community.dto.PostRequest
import com.codecamp.community.repository.CommunityRepository
import com.codecamp.community.repository.PostRepository
import com.codecamp.community.repository.TagRepository
import com.codecamp.community.repository.UserCommunityRepository
import com.codecamp.community.services.PostService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

import java.util.stream.Collectors

@Service
class PostServiceImpl implements PostService {

  @Autowired
  PostRepository postRepository

  @Autowired
  CommunityRepository communityRepository

  @Autowired
  TagRepository tagRepository

  @Autowired
  UserCommunityRepository userCommunityRepository

  @Override
  Post addPost(Long communityId, Long tagId, PostRequest request) {
    Community community = this.communityRepository.findById(communityId)
        .orElseThrow {
          -> new ResourceNotFoundException("can't get community id ${communityId}")
        }
    Tag tag = this.tagRepository.findById(tagId)
        .orElseThrow {
          -> new ResourceNotFoundException("Can't get tag id ${tagId}")
        }
    Post post = new Post()
    post.title = request.title
    post.slug = "${AppUtil.stringToSlug(request.title)}_${new Date().getTime()}"
    post.tag = tag
    post.content = request.content
    post.community = community
    post.createDate = new Date()
    this.postRepository.save(post)
  }

  @Override
  Post getPostDetail(Long id) {
    this.postRepository.findById(id)
        .orElseThrow {
          -> new ResourceNotFoundException("Can't get post id ${id}")
        }
  }

  @Override
  Page<Post> getPosts(Long communityId, Pageable pageable) {
    this.postRepository.findByCommunity_IdOrderByCreateDateDesc(communityId, pageable)
  }

  @Override
  Post getPostBySlug(String slug) {
    this.postRepository.findBySlug(slug)
      .orElseThrow {
        -> new ResourceNotFoundException("Can't get post slug ${slug}")
      }
  }

  @Override
  Page<Post> getPostForHome(Long userId, Pageable pageable) {
    List<UserCommunity> userCommunity = userCommunityRepository.findByUser_Id(userId)
    List<Long> users = userCommunity.stream().map {
      it -> it.user.id
    }.collect(Collectors.toList())
    this.postRepository.findByCommunity_IdInOrderByCreateDateDesc(users, pageable)
  }

}
