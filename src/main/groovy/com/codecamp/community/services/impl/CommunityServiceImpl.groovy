package com.codecamp.community.services.impl

import com.codecamp.common.domains.Community
import com.codecamp.common.domains.Post
import com.codecamp.common.domains.User
import com.codecamp.common.domains.UserCommunity
import com.codecamp.common.exception.ResourceNotFoundException
import com.codecamp.common.utils.AppUtil
import com.codecamp.common.utils.Role
import com.codecamp.common.utils.Status
import com.codecamp.community.repository.CommunityRepository
import com.codecamp.community.repository.UserCommunityRepository
import com.codecamp.community.repository.UserRepository
import com.codecamp.community.services.CommunityService
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.util.stream.Collectors

@Service
@Slf4j
class CommunityServiceImpl implements CommunityService {

  @Autowired
  CommunityRepository communityRepository

  @Autowired
  UserCommunityRepository userCommunityRepository

  @Autowired
  UserRepository userRepository

  @Override
  Community findCommunityById(Long id) {
    communityRepository.findById(id)
        .orElseThrow({ -> new ResourceNotFoundException("Community not found ${id}") })
  }

  @Transactional
  @Override
  Community saveCommunity(Community community, Long userId) {
    Optional<Community> communityOptional = communityRepository.findByName(community.name)
    communityOptional.ifPresent {
      it -> throw new IllegalAccessException("Community name: ${it.name} exists")
    }
    community.status = Status.ACTIVE
    community.slug = AppUtil.stringToSlug(community.name)
    Community c = this.communityRepository.save(community)

    UserCommunity userCommunity = new UserCommunity()
    Community com = new Community()
    com.id = c.id
    userCommunity.community = com
    User user = new User()
    user.id = userId
    userCommunity.user = user
    userCommunity.role = Role.ADMIN
    userCommunityRepository.save(userCommunity)
    community
  }

  @Override
  Community updateStatusDeActive(Long id) {
    Community com = this.communityRepository.findById(id)
        .orElseThrow {
          -> new ResourceNotFoundException("Community not found ${id}")
        }
    if (com.status == Status.DEACTIVE) {
      throw new ResourceNotFoundException("Community not found ${id}")
    }
    com.status = Status.DEACTIVE
    communityRepository.save(com)
    com
  }

  @Override
  Page<Community> findAll(int page, int limit) {
    this.communityRepository.findByStatus(Status.ACTIVE, PageRequest.of(page, limit))
  }

  @Override
  Community updateCommunity(Long id, Community community) {
    Community com = this.communityRepository.findById(id)
        .orElseThrow {
          -> new ResourceNotFoundException("Community not found id ${id}")
        }
    com.name = community.name
    com.slug = AppUtil.stringToSlug(community.name)
    if (community.banner) {
      com.banner = community.banner
    }
    if (community.description) {
      com.description = community.description
    }
    if (community.downVote) {
      com.downVote = community.downVote
    }
    if (community.upVote) {
      com.upVote = community.upVote
    }
    if(community.avatar) {
      com.avatar = community.avatar
    }
    this.communityRepository.save(com)
    com
  }

  @Override
  Community findBySlug(String slug) {
    communityRepository.findBySlug(slug).orElseThrow {
      -> new ResourceNotFoundException("Community not found id ${slug}")
    }
  }

  @Override
  Community undoCommunity(Long id) {
    Community com = this.communityRepository.findById(id)
        .orElseThrow {
          -> new ResourceNotFoundException("Community not found id ${id}")
        }
    if (com.status == Status.DEACTIVE) {
      com.status = Status.ACTIVE
      this.communityRepository.save(com)
      com
    } else {

    }
  }

  @Override
  void deleteCommunity(Long id) {
    Community com = this.communityRepository.findById(id)
        .orElseThrow {
          -> new ResourceNotFoundException("Community not found id ${id}")
        }
    if (com.status == Status.DEACTIVE) {
      this.communityRepository.deleteById(com.id)
      this.log.info("Deleted community id ${com.id}")
    }
  }

  @Override
  UserCommunity joinCommunity(Long id, Long userId) {
    Community com = this.communityRepository.findById(id)
        .orElseThrow {
          -> new ResourceNotFoundException("Community not found id ${id}")
        }
    User user = this.userRepository.findById(userId)
        .orElseThrow {
          -> new ResourceNotFoundException("User not found id ${userId}")
        }
    UserCommunity userCommunity = new UserCommunity()
    userCommunity.role = Role.USER
    userCommunity.community = com
    userCommunity.user = user
    this.userCommunityRepository.save(userCommunity)
  }

  @Override
  List<UserCommunity> getUsersByCommunity(Long communityId) {
    Community com = this.communityRepository.findById(communityId)
      .orElseThrow {
        -> new ResourceNotFoundException("Community not found id ${communityId}")
      }
    Set<UserCommunity> userCommunities = com.userCommunities
    userCommunities.stream().map {
      it -> it
    }.collect(Collectors.toList())
  }


}