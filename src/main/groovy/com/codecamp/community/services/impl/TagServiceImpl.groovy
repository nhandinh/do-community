package com.codecamp.community.services.impl

import com.codecamp.common.domains.Community
import com.codecamp.common.domains.Tag
import com.codecamp.common.exception.ResourceNotFoundException
import com.codecamp.community.repository.CommunityRepository
import com.codecamp.community.repository.TagRepository
import com.codecamp.community.services.TagService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TagServiceImpl implements TagService {

  @Autowired
  TagRepository tagRepository

  @Autowired
  CommunityRepository communityRepository

  @Override
  Tag addTag(Tag tag, Long id) {
    Community community = this.communityRepository.findById(id)
        .orElseThrow {
          -> new ResourceNotFoundException("Community not found ${id}")
        }
    tag.community = community
    this.tagRepository.save(tag)
  }

  @Override
  List<Tag> getTags(Long id) {
    this.tagRepository.findByCommunity_Id(id)
  }
}
