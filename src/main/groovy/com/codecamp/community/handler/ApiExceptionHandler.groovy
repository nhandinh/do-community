package com.codecamp.community.handler

import com.codecamp.common.dto.Result
import com.codecamp.common.exception.BadRequestException
import org.springframework.http.HttpStatus
import org.springframework.validation.BindingResult
import org.springframework.validation.FieldError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
class ApiExceptionHandler {

  @ExceptionHandler(BadRequestException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  Result badRequestException(Exception ex) {
    return new Result().error(ex.message)
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  Result notValidException(MethodArgumentNotValidException ex) {
    BindingResult result = ex.getBindingResult()
    List<FieldError> fieldErrors = result.getFieldErrors()
    return new Result().error(fieldErrors.get(0).defaultMessage)
  }

  @ExceptionHandler(IllegalAccessException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  Result accessException(IllegalAccessException ex) {
    return new Result().error(ex.message)
  }
}
