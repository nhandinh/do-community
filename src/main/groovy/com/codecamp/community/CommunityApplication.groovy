package com.codecamp.community

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@SpringBootApplication
@EnableEurekaClient
@EntityScan('com.codecamp.common.domains')
class CommunityApplication {

  static void main(String[] args) {
    SpringApplication.run CommunityApplication, args
  }
}
