package com.codecamp.community.repository


import com.codecamp.common.domains.Tag
import org.springframework.data.jpa.repository.JpaRepository

interface TagRepository extends JpaRepository<Tag, Long> {

  List<Tag> findByCommunity_Id(Long communityId)
}
