package com.codecamp.community.repository

import com.codecamp.common.domains.Comment
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository

interface CommentRepository extends PagingAndSortingRepository<Comment, Long> {
    Page<Comment> findCommentByParentId(Long parentId, Pageable pageable)

    Page<Comment> findByPost_IdAndParentIdNotNullOrderByCreateDateDesc(Long postId, Pageable pageable)
}
