package com.codecamp.community.repository

import com.codecamp.common.domains.User
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository extends JpaRepository<User, Long> {

  Optional<User> findByEmail(String email)

  Boolean existsByEmail(String email)
}
