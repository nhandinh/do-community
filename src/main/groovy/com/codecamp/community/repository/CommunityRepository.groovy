package com.codecamp.community.repository

import com.codecamp.common.domains.Community
import com.codecamp.common.utils.Status
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository

interface CommunityRepository extends PagingAndSortingRepository<Community, Long> {

  Page<Community> findByStatus(Status status, Pageable pageable)

  Optional<Community> findBySlug(String slug)

  Optional<Community> findByName(String name)

}
