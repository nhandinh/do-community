package com.codecamp.community.repository

import com.codecamp.common.domains.UserCommunity
import org.springframework.data.repository.PagingAndSortingRepository

interface UserCommunityRepository extends PagingAndSortingRepository<UserCommunity, Long> {

  List<UserCommunity> findByUser_Id(Long userId)
}