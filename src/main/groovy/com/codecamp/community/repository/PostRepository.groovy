package com.codecamp.community.repository


import com.codecamp.common.domains.Post
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository

interface PostRepository extends PagingAndSortingRepository<Post, Long> {

  Page<Post> findByCommunity_IdOrderByCreateDateDesc(Long communityId, Pageable Pageable)

  Optional<Post> findBySlug(String slug)

  Page<Post> findByCommunity_IdInOrderByCreateDateDesc(List<Long> ids, Pageable Pageable)
}
